import optparse
import random


class WinnerChoser(object):
    '''
    Class to hold the raffle. It must be provided with a file
    with the names of the participants
    '''
    _N = 1000000
    participants = []

    def __init__(self, input_file=None):
        self.participants = self.load_names(input_file)

    @classmethod
    def load_names(cls, names_file):
        '''
        Class method to load the names from the input file into
        a list
        '''
        names_list = []
        with open(names_file, 'r') as fd:
            while 1:
                a_line = fd.readline()
                if a_line == '':
                    break
                name = a_line.strip()
                names_list.append(name)
        return names_list

    def chose_names(self):
        '''
        Method to perform the raffle. It makes _N random choices and for each
        name chosen, adds +1 point to that participant. In the end, its sorts
        the results by points and retrieves sorted list of tuples
        '''
        print "--> Running raffle"
        results = dict(
                       ((str(x)), 0)
                       for x in self.participants
                       )

        for _ in range(self._N):
            chosen_name = random.choice(self.participants)
            results[chosen_name] += 1

        results = sorted(results.items(),
                         key=lambda (k, v): v,
                         reverse=True)
        print "--> Finished raffle"
        return results

    def format_raffle_results(self, results):
        win = results[0]
        output = "\nThe results of the raffle were:\n\n"
        for r in results:
            output += str(r[0]) + ':' + str(r[1]) + '\n'
        output += '\nAnd the winner is...\n %s, with %s points\n ' % (win[0],
                                                                 str(win[1]))
        return output

    def run_raffle(self):
        results = self.chose_names()
        result_text = self.format_raffle_results(results)
        print result_text
        return


def main():
    p = optparse.OptionParser()
    p.add_option('--input_names', '-i',
                 help='''File with the names of the people
                            participating in the raffle, one per line''')
    options, _ = p.parse_args()
    if not options.input_names:
        print "\nNo input provided. Type -h for help"
        return
    else:
        names_file = str(options.input_names)

    wc = WinnerChoser(names_file)
    wc.run_raffle()


if __name__ == '__main__':
    main()
